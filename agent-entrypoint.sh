# Create the /tmp folder if it does not exist.
TMP_FOLDER=/tmp
[ ! -d "$TMP_FOLDER" ] && mkdir -p $TMP_FOLDER

RCE_AGENT_EXAMPLE_FOLDER=/rce-agent-example
cp ${RCE_AGENT_EXAMPLE_FOLDER}/slow-count.sh $TMP_FOLDER

cd $RCE_AGENT_EXAMPLE_FOLDER
./server --addr 0.0.0.0:5501
