#!/bin/sh

IMAGE_NAME=$1
INCREASE_GRPC_VERBOSITY="GRPC_GO_LOG_VERBOSITY_LEVEL=99 GRPC_GO_LOG_SEVERITY_LEVEL=info"
LOCAL_CLIENT_PATH=$2

printTestHeader() {
    echo $1
    echo ----------
}

printTestFooter() {
    echo ----------
    echo Done!
    echo
}

testSingleContainer() {
    printTestHeader 'Testing with a single container...'

    echo Starting the RCE Agent container...
    RCE_AGENT_CONTAINER_ID=$(docker run -d --rm $IMAGE_NAME /bin/bash -c ./agent-entrypoint.sh)
    echo

    docker exec $RCE_AGENT_CONTAINER_ID /bin/bash -c "$INCREASE_GRPC_VERBOSITY ./client ls-tmp" 
    echo

    echo Stopping the RCE Agent container...
    docker container stop $RCE_AGENT_CONTAINER_ID

    unset RCE_AGENT_CONTAINER_ID

    printTestFooter
}

testLocalClient() {
    if [ -z "$LOCAL_CLIENT_PATH" ]; then
        echo The local client path is missing, so will skip \"testLocalClient\"!
        echo && return
    fi

    printTestHeader 'Testing with a local client...'

    echo Starting the RCE Agent container...
    RCE_AGENT_CONTAINER_ID=$(docker run -d --rm $IMAGE_NAME /bin/bash -c ./agent-entrypoint.sh)

    RCE_AGENT_CONTAINER_IP_ADDR=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $RCE_AGENT_CONTAINER_ID)
    echo RCE Agent container IP is $RCE_AGENT_CONTAINER_IP_ADDR
    RCE_AGENT_ADDRESS="${RCE_AGENT_CONTAINER_IP_ADDR}:5501"
    echo RCE Agent address is $RCE_AGENT_ADDRESS
    echo

    bash -c "$INCREASE_GRPC_VERBOSITY $LOCAL_CLIENT_PATH --server-addr $RCE_AGENT_ADDRESS ls-tmp" 
    echo

    echo Stopping the RCE Agent container...
    docker container stop $RCE_AGENT_CONTAINER_ID

    unset RCE_AGENT_CONTAINER_ID

    printTestFooter
}

testLocalClientPortMapping() {
    if [ -z "$LOCAL_CLIENT_PATH" ]; then
        echo The local client path is missing, so will skip \'testLocalClientPortMapping\'!
        echo && return
    fi

    printTestHeader 'Testing with a local client + port mapping...'

    echo Starting the RCE Agent container...
    RCE_AGENT_CONTAINER_ID=$(docker run -d -p 5501:5501 --rm $IMAGE_NAME /bin/bash -c ./agent-entrypoint.sh)
    echo

    bash -c "$INCREASE_GRPC_VERBOSITY $LOCAL_CLIENT_PATH ls-tmp" 
    echo

    echo Stopping the RCE Agent container...
    docker container stop $RCE_AGENT_CONTAINER_ID

    unset RCE_AGENT_CONTAINER_ID

    printTestFooter
}

testSharedVolume() {
    printTestHeader 'Testing with a shared volume...'

    RCE_FILES_CONTAINER_ID=$(docker run -d -v /rce-agent-example $IMAGE_NAME)
    echo

    echo Starting the RCE Agent container...
    RCE_AGENT_CONTAINER_ID=$(docker run -d --rm --volumes-from $RCE_FILES_CONTAINER_ID debian:buster-slim /bin/bash -c /rce-agent-example/agent-entrypoint.sh)

    RCE_AGENT_CONTAINER_IP_ADDR=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $RCE_AGENT_CONTAINER_ID)
    echo RCE Agent container IP is $RCE_AGENT_CONTAINER_IP_ADDR
    RCE_AGENT_ADDRESS="${RCE_AGENT_CONTAINER_IP_ADDR}:5501"
    echo RCE Agent address is $RCE_AGENT_ADDRESS
    echo

    echo Running the RCE Client from a new container...
    docker run --rm $IMAGE_NAME /bin/bash -c "hostname -I | awk '{print $1}' && $INCREASE_GRPC_VERBOSITY ./client --server-addr $RCE_AGENT_ADDRESS slow-count" 
    echo

    echo Stopping the RCE Agent container...
    docker container stop $RCE_AGENT_CONTAINER_ID

    echo Removing the RCE Files container...
    docker container rm $RCE_FILES_CONTAINER_ID

    unset RCE_FILES_CONTAINER_ID
    unset RCE_AGENT_CONTAINER_ID
    unset RCE_AGENT_CONTAINER_IP_ADDR
    unset RCE_AGENT_ADDRESS

    printTestFooter
}

echo Image\'s /rce-agent-example folder contains the files bellow:
docker run --rm $IMAGE_NAME /bin/bash -c "ls -la /rce-agent-example" 
echo

testSingleContainer
testLocalClient
testLocalClientPortMapping
testSharedVolume
