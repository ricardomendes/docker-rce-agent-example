# ============================================================================ #
# The client and server are built in the first stage.                          #
# ============================================================================ #
FROM golang:1.14 AS build

WORKDIR /build

RUN git clone https://github.com/square/rce-agent.git \
    && cd ./rce-agent/example/client \
    && go build \
    && cd ../server \
    && go build

# ============================================================================ #
# Build the lightweight resulting image.                                       #
# ============================================================================ #
FROM debian:buster-slim

WORKDIR /rce-agent-example

COPY --from=build /build/rce-agent/example/client/client .

COPY --from=build /build/rce-agent/example/server/server .
COPY --from=build /build/rce-agent/example/server/commands.yaml .
COPY --from=build /build/rce-agent/example/server/slow-count.sh .
COPY agent-entrypoint.sh .

RUN chmod +x agent-entrypoint.sh
