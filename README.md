# Docker image with rce-agent examples

An OCI-compliant image to test
[github.com/square/rce-agent](https://github.com/square/rce-agent) capabilities
in containerized environments.

[![pipeline
status](https://gitlab.com/ricardomendes/docker-rce-agent-example/badges/master/pipeline.svg)](https://gitlab.com/ricardomendes/docker-rce-agent-example/-/commits/master)

## Testing the image

```sh
chmod +x test-image.sh

./test-image.sh <IMAGE-NAME> [PATH-TO-THE-LOCAL-CLIENT-BINARY]
```

Test script arguments:

| Name                                | Description                                                   | Mandatory? |
| ----------------------------------- | ------------------------------------------------------------- | :--------: |
| **IMAGE-NAME**                      | The image to be used in the tests.                            |    yes     |
| **PATH-TO-THE-LOCAL-CLIENT-BINARY** | Enable the tests that depend on a local client binary to run. |     no     |
